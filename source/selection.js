var lastSelection = "";
var shouldILog = false;

$(window).keyup(function(e)
{ 
    log("key up");

    if(e.key=="Escape"){
        log("esc key up - removing selection");
        removeSelection();
    }
});

function log(text){
    if(shouldILog)
        console.log(text);
}

function removeSelection(){
    $('body').removeHighlight();
    lastSelection = '';
}

function printSelectionIfNew(){
    var selection = window.getSelection().toString();
    
    log("## Selection: " + selection);

    if (selection == '')
    {
        removeSelection();
    }
    else if(selection !== lastSelection && selection != '')
    {
        lastSelection = selection;
        removeSelection();
        $('body').highlight(selection);
    }
}

document.body.onmouseup = function(e) {
    log("Mouse button UP");
    
    if(e.metaKey || e.ctrlKey) {
        log("Mouse button UP with CTRL");
        printSelectionIfNew();
    }
}
